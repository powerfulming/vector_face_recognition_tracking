from face_recognizer import FaceRecognizer
import zmq
import cv2
import numpy as np
from multiprocessing import shared_memory
from utils import check_python_version
from utils import KeyboardEventDetect

import threading
import image_memory_address_pb2

stop_flag_ = False

def zmq_server_thread():
    global image_, stop_flag_        
    
    context = zmq.Context()
    zmqSocketServer = context.socket(zmq.REP)
    zmqSocketServer.bind("tcp://*:5555")
    # Setup Waiting Timeout
    zmqSocketServer.RCVTIMEO = 1000
    print("Created ZMQ Server")
    
    # create FaceRecognizer to detect and recognize face
    face_recognizer = FaceRecognizer()

    while not stop_flag_:
        # ZMQ Server try to wait protobuf message. If timeout, wait again.
        # Timeout is necessary because stop_flag_ need to be updated
        try:
            messageInBytes = zmqSocketServer.recv()
        except:
            # waiting client request timeout
            continue

        # Parse Image by protobuf
        image_pb = image_memory_address_pb2.Image()
        image_pb.ParseFromString(messageInBytes)
        existingSharedMemory = shared_memory.SharedMemory(image_pb.image_addr)
        columns = image_pb.cols
        rows=image_pb.rows
        size = columns*rows

        # convert to opencv image format
        image = np.ndarray((rows,columns, 3), dtype=np.uint8, buffer=existingSharedMemory.buf)
        
        # Use face_recognizer to find face, then identify the face
        face_recognizer.set_image(image)
        face_recognizer.dlib_hog_svm_detect_face()
        faceList = face_recognizer.dlib_recognize_face()
        if len(faceList)>0:
            for i in range(len(faceList)):
                print('[%d] Name: %s x: %d y: %d' %(i, faceList[i].name, faceList[i].x, faceList[i].y))
                cv2.rectangle(image, (faceList[i].x, faceList[i].y), (faceList[i].x + faceList[i].w, faceList[i].y + faceList[i].h), (0, 255, 0), 2)
                cv2.putText(image, faceList[i].name, (faceList[i].x, faceList[i].y), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (0, 255, 0), 2)
        else:
            print('no face')
        zmqSocketServer.send(b'Hello World')
        cv2.imshow("display", image)
        key = cv2.waitKey(1)
    cv2.destroyAllWindows()


def main():
    if not check_python_version('3.10.5'):
        print('Wrong version. Please go to environment vairable editor of Windows and put \n\
        \'C:\\Users\\jianm\\AppData\\Local\\Programs\\Python\\Python310\\\'\n\
on the top')
        print('<----------- Finish program ---------->')
        return

    global stop_flag_
    keyboardDetectThread = KeyboardEventDetect()
    keyboardDetectThread.start()

    # Start a thread to deal with the ZMQ client request
    zmqServerThread = threading.Thread(target = zmq_server_thread)
    zmqServerThread.start()

    keyboardDetectThread.join()
    stop_flag_ = keyboardDetectThread.stop_flag_    
    zmqServerThread.join()

if __name__ == "__main__":
    main()