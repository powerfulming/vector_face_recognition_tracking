# Vector with Face Recognition
```py
import anki_vector
import threading
import numpy as np
import cv2
from PIL import Image

args = anki_vector.util.parse_command_args()

def camera_callback(robot):
    while True:
        image = robot.camera.latest_image
        image_np = np.asanyarray(image.raw_image)
        image_cv = cv2.cvtColor(image_np, cv2.COLOR_RGB2BGR)
        cv2.imshow("Vector Camera",image_cv)
        if cv2.waitKey(100) == 27:
            break

if __name__ == "__main__":
    with anki_vector.AsyncRobot(args.serial,
                           show_viewer=True) as robot:
        # Invoke the ipython shell while connected to Vector
        print('Hello world')
        robot.camera.init_camera_feed()
        cameraThread = threading.Thread(target = camera_callback, args=[robot])
        
        # print(image.raw_image)
        
        # image_pil = Image.fromarray(image_np)
        # print(type(image_np))
        # print(image_np)
        
        cameraThread.join()
        cv2.destroyAllWindows()
```
```py
python $anki_tutorial_program --serial SERIAL
```