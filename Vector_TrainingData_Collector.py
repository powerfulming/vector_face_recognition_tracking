import keyboard
import zmq
import cv2
import numpy as np
from multiprocessing import shared_memory
import image_memory_address_pb2
import utils
from utils import KeyboardEventDetect
import threading
from face_recognizer import FaceRecognizer

stop_flag_ = False
counter_ = 0

def request_face_recognize_loop(zmqSocketClient, face_recognizer):
    global stop_flag_, counter_
    while not stop_flag_:
        print(f"Sending request {'a'} …")
        zmqSocketClient.send(b"Hello")

        #  Get the reply.
        try:
            messageInBytes = zmqSocketClient.recv()
        except:
            # waiting server response timeout
            continue
        print('Type of receiving message: ', type(messageInBytes))
        image_pb = image_memory_address_pb2.Image()
        image_pb.ParseFromString(messageInBytes)

        existingSharedMemory = shared_memory.SharedMemory(image_pb.image_addr)
        columns = image_pb.cols
        rows=image_pb.rows
        size = columns*rows

        image = np.ndarray((rows,columns, 3), dtype=np.uint8, buffer=existingSharedMemory.buf)

        if keyboard.is_pressed('enter'):
            face_recognizer.set_image(image)
            faceList = face_recognizer.dlib_hog_svm_detect_face()
            name = 'louis_'+str(counter_)+'.jpg'
            if len(faceList)>0:
                cv2.imwrite(name, image)
                counter_ = counter_+1
            else:
                print('no face')
        cv2.imshow("display", image)
        key = cv2.waitKey(1)

def main():
    if not utils.check_python_version('3.10.5'):
        print('Wrong version. Please go to environment vairable editor of Windows and put \n\
        \'C:\\Users\\jianm\\AppData\\Local\\Programs\\Python\\Python310\\\'\n\
on the top')
        print('<----------- Finish program ---------->')
        return
    
    global stop_flag_
    keyboardDetectThread = KeyboardEventDetect()
    keyboardDetectThread.start()

    # create FaceRecognizer to detect and recognize face
    face_recognizer = FaceRecognizer()
    
    context = zmq.Context()
    zmqSocketClient = context.socket(zmq.REQ)
    zmqSocketClient.connect("tcp://127.0.0.1:5555")
    # Setup timeout for zmq socket client when waiting server response
    zmqSocketClient.RCVTIMEO = 1000
    zmqSocketClientThread = threading.Thread(target=request_face_recognize_loop, args=[zmqSocketClient, face_recognizer])
    zmqSocketClientThread.start()
    print("Create ZMQ Client")
    

    keyboardDetectThread.join()
    stop_flag_ = keyboardDetectThread.stop_flag_
    zmqSocketClientThread.join()
    print('<----------- End Client ------------>')

if __name__ == "__main__":
    main()
