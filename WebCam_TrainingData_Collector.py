import keyboard
import cv2
import numpy as np
import utils
from utils import KeyboardEventDetect
import threading
from face_recognizer import FaceRecognizer

stop_flag_ = False
counter_ = 0
image_ = np.ndarray([360, 640, 3])
WEBCAM_START_ = False

def webcam_callback():
    global image_, stop_flag_, WEBCAM_START_
    video = cv2.VideoCapture(0)
    if not video.isOpened():
        video.open(0)
    print('open camera')
    WEBCAM_START_ = True
    while not stop_flag_:
        success, image = video.read()
        image_ = cv2.resize(image, (640, 360), interpolation=cv2.INTER_AREA)
        cv2.waitKey(1)
    print('finish webcam_callback')


def request_face_recognize_loop(face_recognizer):
    global stop_flag_, counter_, image_
    counter=0
    while not stop_flag_:
        if not WEBCAM_START_:
            counter=counter+1
            if counter>10:
                print('waiting camera open')
                counter=0
            continue
        if keyboard.is_pressed('enter'):
            face_recognizer.set_image(image_)
            faceList = face_recognizer.dlib_hog_svm_detect_face()
            name = 'louis_'+str(counter_)+'.jpg'
            if len(faceList)>0:
                cv2.imwrite(name, image_)
                counter_ = counter_+1
            else:
                print('no face')
        cv2.imshow("display", image_)
        key = cv2.waitKey(1)

def main():
    if not utils.check_python_version('3.10.5'):
        print('Wrong version. Please go to environment vairable editor of Windows and put \n\
        \'C:\\Users\\jianm\\AppData\\Local\\Programs\\Python\\Python310\\\'\n\
on the top')
        print('<----------- Finish program ---------->')
        return
    
    global stop_flag_
    keyboardDetectThread = KeyboardEventDetect()
    keyboardDetectThread.start()

    webcamThread = threading.Thread(target=webcam_callback)
    webcamThread.start()

    # create FaceRecognizer to detect and recognize face
    face_recognizer = FaceRecognizer()
    
    dataCollectThread = threading.Thread(target=request_face_recognize_loop, args=[face_recognizer])
    dataCollectThread.start()
    print("Create ZMQ Client")
    

    keyboardDetectThread.join()
    stop_flag_ = keyboardDetectThread.stop_flag_
    dataCollectThread.join()
    webcamThread.join()
    print('<----------- End Client ------------>')

if __name__ == "__main__":
    main()
