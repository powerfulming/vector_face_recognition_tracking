from glob import glob
import zmq
import anki_vector
import threading
import cv2
import numpy as np
import keyboard
import time
from multiprocessing import shared_memory
import image_memory_address_pb2
import utils
from utils import KeyboardEventDetect

args = anki_vector.util.parse_command_args()
stop_flag_ = False
image_ = np.ndarray([360, 640, 3])
USE_WEBCAM_ = True
WEBCAM_START_ = False

def camera_callback(robot):
    global stop_flag_, image_
    while not stop_flag_:
        image = robot.camera.latest_image
        image_np = np.asanyarray(image.raw_image)
        image_ = cv2.cvtColor(image_np, cv2.COLOR_RGB2BGR)
        # cv2.imshow("Louis Vector Camera",image_)
        # cv2.waitKey(1)
    print('finish camera_callback')

def zmq_client_thread():
    global image_, stop_flag_, WEBCAM_START_

    counter = 0
    if USE_WEBCAM_:
        while not WEBCAM_START_:
            if counter > 100:
                counter = 0
                print('Waiting webcam start')
            counter = counter+1
            time.sleep(0.01)
    
    context = zmq.Context()
    zmqSocketClient = context.socket(zmq.REQ)
    zmqSocketClient.connect("tcp://127.0.0.1:5555")
    # Setup Waiting Timeout
    zmqSocketClient.RCVTIMEO = 1000
    print("Created ZMQ Client")
    timeoutFlag = False
    while not stop_flag_:
        if not timeoutFlag:
            cv2.imshow("CameraViewer",image_)
            cv2.waitKey(1)
            # Store Message to share memory
            imageBytes = image_.tobytes()
            size = len(imageBytes)
            img_bytearray = bytearray(imageBytes)
            shmA=shared_memory.SharedMemory(create=True, size=size)
            shmA.buf[:size] = img_bytearray
            print('Shared Memory Name: %s' % (shmA.name))

            # Create ProtoBuf Message
            image_pb = image_memory_address_pb2.Image()
            image_pb.cols = image_.shape[1]
            image_pb.rows = image_.shape[0]
            image_pb.timestamp = int(time.time()) # float to int
            image_pb.image_addr = shmA.name
            message = image_pb.SerializeToString()
            #  Send reply back to client
            zmqSocketClient.send(message)
        #  Get the reply.
        try:
            messageInBytes = zmqSocketClient.recv()
            print(f"Received request: {messageInBytes}")
            timeoutFlag = False
        except:
            # waiting server response timeout
            print("Waiting server response timeout")
            timeoutFlag = True
            continue
    zmqSocketClient.close()
    context.term()
    print('finish zmq_server_thread')

def vector_monitor_thread():
    global stop_flag_
    try:
        with anki_vector.AsyncRobot(args.serial,
                    show_viewer=False) as robot:
            print('Connected to Anki Vector')
            for i in range(3):
                # lift motor speed:
                # fast: 8
                # mid: 4
                # slow: 2
                robot.motors.set_lift_motor(2)
                robot.motors.set_head_motor(-1)
                time.sleep(1)
                robot.motors.set_head_motor(1)
                robot.motors.set_lift_motor(-2)
                time.sleep(1)
                
            robot.camera.init_camera_feed()
            cameraThread = threading.Thread(target = camera_callback, args=[robot])
            cameraThread.start()
            cameraThread.join()
            print('join vector_monitor_thread')

    except KeyboardInterrupt:
        print("Interrupt received, stopping...")
        stop_flag_ = True        

def webcam_callback():
    global image_, stop_flag_, WEBCAM_START_
    video = cv2.VideoCapture(0)
    if not video.isOpened():
        video.open(0)
    print('open camera')
    WEBCAM_START_ = True
    while not stop_flag_:
        success, image = video.read()
        image_ = cv2.resize(image, (640, 360), interpolation=cv2.INTER_AREA)
        cv2.waitKey(1)
    print('finish webcam_callback')

def main():
    global stop_flag_, USE_WEBCAM_

    if not USE_WEBCAM_:
        if not utils.check_python_version('3.8.10'):
            print('Wrong version. Please go to environment vairable editor of Windows and put \n\
            \'C:\\Users\\jianm\\.pyenv\\pyenv-win\\bin\\\'\n\
            \'C:\\Users\\jianm\\.pyenv\\pyenv-win\\shims\\ \' \n\
             on the top')
            print('Then run \'pyenv global 3.8.10\'')
            print('<----------- Finish program ---------->')
            return

    # Start a thread to detect keyboard input to interrupt the program
    keyboardDetectThread = KeyboardEventDetect()
    keyboardDetectThread.start()

    # Start a thread to deal with the ZMQ client request
    zmqServerThread = threading.Thread(target = zmq_client_thread)
    zmqServerThread.start()
    
    if USE_WEBCAM_:
        webcamThread = threading.Thread(target=webcam_callback)
        webcamThread.start()
    else:
        # Start a thread to communication with anki vector
        vectorCommunicationThread = threading.Thread(target=vector_monitor_thread)
        vectorCommunicationThread.start()
    
    print("Waiting thread join")
    keyboardDetectThread.join()
    stop_flag_ = keyboardDetectThread.stop_flag_    
    zmqServerThread.join()
    if USE_WEBCAM_:
        webcamThread.join()
    else:
        vectorCommunicationThread.join()
    cv2.destroyAllWindows()

if __name__ == "__main__":
    main()
