from platform import python_version
import keyboard
from threading import Thread
import cv2
import numpy as np
import time

def check_python_version(targetVersion) -> bool:
    currentVersion = python_version()
    if currentVersion!= targetVersion:
        print('Current Python version is %s' %(currentVersion))
        return False
    else:
        return True

class KeyboardEventDetect(Thread):
    # constructor
    def __init__(self):
        # execute the base constructor
        Thread.__init__(self)
        self.stop_flag_ = False
    
    # function executed in a new thread
    def run(self):
        while not self.stop_flag_:  # making a loop
            if keyboard.is_pressed('q'):  # if key 'q' is pressed 
                print('You Pressed Q ! Quit the program!')
                self.stop_flag_ = True
                break

class WebCamThread(Thread):
    # constructor
    def __init__(self):
        # execute the base constructor
        Thread.__init__(self)
        self.stop_flag_ = False
        self.webcam_ready_ = False
        # self.image_ = np.ndarray([1080, 1920, 3])
        self.image_ = np.zeros([480, 640, 3])
        self.video_ = cv2.VideoCapture(0, cv2.CAP_DSHOW)
        if not self.video_.isOpened():
            self.video_.open(0)
        print('open camera')
        self.webcam_ready_ = True
    
    # update Stop_Flag_ to decide when to stop module
    def update_stop_flag(self, stopFlag):
        self.stop_flag_ = stopFlag

    def get_image(self):
        while not self.webcam_ready_:
            time.sleep(1)
        return self.image_

    # function executed in a new thread
    def run(self):
        while not self.stop_flag_:  # making a loop
            success, self.image_ = self.video_.read()
            print(self.image_.shape)
            