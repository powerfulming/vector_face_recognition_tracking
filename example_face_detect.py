from face_recognizer import FaceRecognizer
import cv2
from utils import KeyboardEventDetect
from utils import WebCamThread
import threading
stop_flag_ = False

def face_detect_callback(webcamThread):

    face_recognizer = FaceRecognizer()
    global stop_flag_
    while not stop_flag_:
        image = webcamThread.get_image()
        face_recognizer.set_image(image)
        faceList = []
        faceList = face_recognizer.dlib_hog_svm_detect_face()
        face_recognizer.show_face(faceList)

def opencv_face_detect_callback(webcamThread):
    face_cascade = cv2.CascadeClassifier('.\\haarcascades_cuda\\haarcascade_frontalface_default.xml')
    global stop_flag_
    while not stop_flag_:
        image = webcamThread.get_image()
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        faces = face_cascade.detectMultiScale(gray,
                                            scaleFactor=1.2,
                                            minNeighbors=3,)
        for (x,y,w,h) in faces:
            img = cv2.rectangle(image,(x,y),(x+w,y+h),(255,0,0),2)

        cv2.imshow("Frame", img)
        cv2.waitKey(1)

def main():
    keyboardDetectThread = KeyboardEventDetect()
    keyboardDetectThread.start()

    webcamThread = WebCamThread()
    webcamThread.start()

    # with DLIB
    # faceDetecThread = threading.Thread(target=face_detect_callback, args=[webcamThread])
    # faceDetecThread.start()

    # with OpenCV
    cvFaceDetecThread = threading.Thread(target=face_detect_callback, args=[webcamThread])
    cvFaceDetecThread.start()

    keyboardDetectThread.join()
    global stop_flag_
    stop_flag_ = keyboardDetectThread.stop_flag_
    webcamThread.update_stop_flag(stop_flag_)
    webcamThread.join()
    # faceDetecThread.join()
    cvFaceDetecThread.join()

if __name__ == "__main__":
    main()