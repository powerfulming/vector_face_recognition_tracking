import imutils
import pickle
import time
import cv2
import os
# Draw image
from matplotlib import pyplot as plt
# Detect Face
import dlib
# Recognize Face
import face_recognition
import numpy as np

class FacePosition:
    def __init__(self):
        self._x = 0
        self._y = 0
        self._w = 0
        self._h = 0
        self._name = ""
    
    # x
    @property
    def x(self):
        return self._x
    @x.setter
    def x(self, intputX):
        self._x = intputX

    # y
    @property
    def y(self):
        return self._y
    @y.setter
    def y(self, intputY):
        self._y = intputY

    # w
    @property
    def w(self):
        return self._w
    @w.setter
    def w(self, intputW):
        self._w = intputW

    # h
    @property
    def h(self):
        return self._h
    @h.setter
    def h(self, intputH):
        self._h = intputH
    
    # name
    @property
    def name(self):
        return self._name
    @name.setter
    def name(self, intputName):
        self._name = intputName

class FaceRecognizer:
    def __init__(self):
        #find path of xml file containing haarcascade file 
        cascPathface = os.path.dirname(cv2.__file__) + "/data/haarcascade_frontalface_alt2.xml"
        # load the harcaascade in the cascade classifier
        self.faceCascade = cv2.CascadeClassifier(cascPathface)
        # load the known faces and embeddings saved in last file
        self.data = pickle.loads(open('face_enc', "rb").read())
        self.face_list = list()
        print('Complete Init FaceRecognizer')

    def set_image(self, image):
        self.image_bgr = image.astype(np.uint8)
        self.image_rgb = cv2.cvtColor(self.image_bgr, cv2.COLOR_BGR2RGB)

    # def show(self):
    #     cv2.imshow("Frame", self.image_bgr)
    #     cv2.waitKey(0)
    #     cv2.destroyAllWindows()
    
    def show(self, image):
        cv2.imshow("Frame", image)
        cv2.waitKey(1)

    # def plt_show(self):
    #     # Convert from BGR to RGB
    #     plt.imshow(self.image_rgb)
    #     plt.show()
    
    def plt_show(self, image):
        plt.imshow(image)
        plt.show()
    
    def dlib_show_face(self):
        win = dlib.image_window()
        win.clear_overlay()
        win.set_image(self.image_rgb)
        win.add_overlay(self.dlib_dets)
        dlib.hit_enter_to_continue()

    def cv_show_face(self):
        imgRect = self.image_bgr
        for bb in self.bounding_box_list:
            (x, y, w, h) = bb
            cv2.rectangle(imgRect, (x, y), (x+w, y+h), (0, 255, 0))
        self.show(imgRect)

    def show_face(self, faceList):
        imgRect = self.image_bgr
        for tmpFace in faceList:
            x = tmpFace.x
            y = tmpFace.y
            w = tmpFace.w
            h = tmpFace.h
            
            cv2.rectangle(imgRect, (x, y), (x+w, y+h), (0, 255, 0))
        self.show(imgRect)

    def dlib_hog_svm_detect_face(self):
        self.face_list.clear()
        # HOG + Linear SVM
        detector = dlib.get_frontal_face_detector()
        self.dlib_dets = detector(self.image_rgb, 1)
        print("Number of faces detected: {}".format(len(self.dlib_dets)))
        for i, d in enumerate(self.dlib_dets):
            print("Detection {}: Left: {} Top: {} Right: {} Bottom: {}".format(
                i, d.left(), d.top(), d.right(), d.bottom()))
            tmpFace = FacePosition()
            (tmpFace.x, tmpFace.y, tmpFace.w, tmpFace.h) = self.rect_to_bounding_box(d)
            tmpFace.name = "Unknown"
            self.face_list.append(tmpFace)
        return self.face_list

    def dlib_cnn_detect_face(self):
        detector = dlib.cnn_face_detection_model_v1('model\\mmod_human_face_detector.dat')
        
        self.dlib_dets = detector(self.image_rgb, 1)
        print("Number of faces detected: {}".format(len(self.dlib_dets)))
        self.bounding_box_list = list()
        for i, d in enumerate(self.dlib_dets):
            print("Detection {}: Left: {} Top: {} Right: {} Bottom: {}".format(
                i, d.rect.left(), d.rect.top(), d.rect.right(), d.rect.bottom()))
            self.bounding_box_list.append(self.rect_to_bounding_box(d.rect))
    
    def rect_to_bounding_box(self, rect):
        # opencv bounding box is (x, y, width, height)
        x = rect.left()
        y = rect.top()
        w = rect.right()-x
        h = rect.bottom()-y
        return (x, y, w, h)

    def dlib_recognize_face(self):
        if len(self.face_list) == 0:
            print('no face in picture')
        print('length of faces: ', len(self.face_list))
        counter=0
        names = []
        for face in self.face_list:
            x = face.x
            y = face.y
            x_w = face.w+x
            y_h = face.h+y
            faceImg = self.image_bgr[y:y_h, x:x_w]
            # self.show(faceImg)
        
            # convert the input frame from BGR to RGB 
            faceRgb = cv2.cvtColor(faceImg, cv2.COLOR_BGR2RGB)
            # the facial embeddings for face in input
            encodings = face_recognition.face_encodings(faceRgb)
            # loop over the facial embeddings incase
            # we have multiple embeddings for multiple fcaes
            print('length of encodings: ',len(encodings))
            name = "Unknown"
            # set True for debug to find why so slow
            debugFlag = True
            if not debugFlag:
                for encoding in encodings:
                #Compare encodings with encodings in data["encodings"]
                #Matches contain array with boolean values and True for the embeddings it matches closely
                #and False for rest
                    matches = face_recognition.compare_faces(self.data["encodings"],
                        encoding, tolerance=0.3)
                    #set name =inknown if no encoding matches
                    # check to see if we have found a match
                    if True in matches:
                        #Find positions at which we get True and store them
                        matchedIdxs = [i for (i, b) in enumerate(matches) if b]
                        counts = {}
                        # loop over the matched indexes and maintain a count for
                        # each recognized face face
                        for i in matchedIdxs:
                            #Check the names at respective indexes we stored in matchedIdxs
                            name = self.data["names"][i]
                            #increase count for the name we got
                            counts[name] = counts.get(name, 0) + 1
                        #set name which has highest count
                        name = max(counts, key=counts.get)
            face.name = name
            # update the list of names
            names.append(name)
            print('counter', counter)
            print('name:', name)
            counter=counter+1

        for name in names:
            print(name)
        print('finish')
        return self.face_list


    # def get_face_location(self):
    #     return (x,y)

    # def recognize_face(self, image):
    #     gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    #     faces = self.faceCascade.detectMultiScale(gray,
    #                                         scaleFactor=1.1,
    #                                         minNeighbors=5,
    #                                         minSize=(60, 60),
    #                                         flags=cv2.CASCADE_SCALE_IMAGE)
    #     # convert the input frame from BGR to RGB 
    #     rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    #     # the facial embeddings for face in input
    #     encodings = face_recognition.face_encodings(rgb)
    #     names = []
    #     # loop over the facial embeddings incase
    #     # we have multiple embeddings for multiple fcaes
    #     for encoding in encodings:
    #     #Compare encodings with encodings in data["encodings"]
    #     #Matches contain array with boolean values and True for the embeddings it matches closely
    #     #and False for rest
    #         matches = face_recognition.compare_faces(self.data["encodings"],
    #         encoding, tolerance=0.3)
    #         #set name =inknown if no encoding matches
    #         name = "Unknown"
    #         # check to see if we have found a match
    #         if True in matches:
    #             #Find positions at which we get True and store them
    #             matchedIdxs = [i for (i, b) in enumerate(matches) if b]
    #             counts = {}
    #             # loop over the matched indexes and maintain a count for
    #             # each recognized face face
    #             for i in matchedIdxs:
    #                 #Check the names at respective indexes we stored in matchedIdxs
    #                 name = self.data["names"][i]
    #                 #increase count for the name we got
    #                 counts[name] = counts.get(name, 0) + 1
    #             #set name which has highest count
    #             name = max(counts, key=counts.get)
                
    
    #         # update the list of names
    #         names.append(name)
    #         # loop over the recognized faces
    #         for ((x, y, w, h), name) in zip(faces, names):
    #             # rescale the face coordinates
    #             # draw the predicted face name on the image
    #             cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 2)
    #             cv2.putText(image, name, (x, y), cv2.FONT_HERSHEY_SIMPLEX,
    #             0.75, (0, 255, 0), 2)

def main():
    # image = cv2.imread("D:\\face_recognition\\images\\mango\\mango.jpg")
    image = cv2.imread("D:\\face_recognition\\images\\mango\\mango_lui.jpg")
    # image = cv2.imread("D:\\face_recognition\\images\\others\\lyingDownPeople.png")
    width = int(image.shape[0]/4)
    height = int(image.shape[1]/4)
    image2 = cv2.resize(image, (height, width), interpolation=cv2.INTER_NEAREST)

    face_recognizer = FaceRecognizer()
    face_recognizer.set_image(image2)
    # face_recognizer.show()
    # face_recognizer.plt_show()
    face_recognizer.dlib_hog_svm_detect_face()
    # face_recognizer.dlib_cnn_detect_face()
    # face_recognizer.cv_show_face()
    # face_recognizer.dlib_show_face()
    # face_recognizer.cv_show_face()
    faceList = face_recognizer.dlib_recognize_face()
    if len(faceList)>0:
        for i in range(len(faceList)):
            print('[%d] Name: %s x: %d y: %d' %(i, faceList[i].name, faceList[i].x, faceList[i].y))
            cv2.rectangle(image2, (faceList[i].x, faceList[i].y), (faceList[i].x + faceList[i].w, faceList[i].y + faceList[i].h), (0, 255, 0), 2)
            cv2.putText(image2, faceList[i].name, (faceList[i].x, faceList[i].y), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (0, 255, 0), 2)
    else:
        print('no face')
    print('the end')
    cv2.imshow("display", image2)
    cv2.waitKey(1)

if __name__ == "__main__":
    main()

# print("Streaming started")
# video_capture = cv2.VideoCapture(0)
# # loop over frames from the video file stream
# while True:
#     # grab the frame from the threaded video stream
#     ret, frame = video_capture.read()
#     gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
#     faces = faceCascade.detectMultiScale(gray,
#                                          scaleFactor=1.1,
#                                          minNeighbors=5,
#                                          minSize=(60, 60),
#                                          flags=cv2.CASCADE_SCALE_IMAGE)
 
#     # convert the input frame from BGR to RGB 
#     rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
#     # the facial embeddings for face in input
#     encodings = face_recognition.face_encodings(rgb)
#     names = []
#     # loop over the facial embeddings incase
#     # we have multiple embeddings for multiple fcaes
#     for encoding in encodings:
#        #Compare encodings with encodings in data["encodings"]
#        #Matches contain array with boolean values and True for the embeddings it matches closely
#        #and False for rest
#         matches = face_recognition.compare_faces(data["encodings"],
#          encoding, tolerance=0.3)
#         #set name =inknown if no encoding matches
#         name = "Unknown"
#         # check to see if we have found a match
#         if True in matches:
#             #Find positions at which we get True and store them
#             matchedIdxs = [i for (i, b) in enumerate(matches) if b]
#             counts = {}
#             # loop over the matched indexes and maintain a count for
#             # each recognized face face
#             for i in matchedIdxs:
#                 #Check the names at respective indexes we stored in matchedIdxs
#                 name = data["names"][i]
#                 #increase count for the name we got
#                 counts[name] = counts.get(name, 0) + 1
#             #set name which has highest count
#             name = max(counts, key=counts.get)
            
 
#         # update the list of names
#         names.append(name)
#         # loop over the recognized faces
#         for ((x, y, w, h), name) in zip(faces, names):
#             # rescale the face coordinates
#             # draw the predicted face name on the image
#             cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
#             cv2.putText(frame, name, (x, y), cv2.FONT_HERSHEY_SIMPLEX,
#              0.75, (0, 255, 0), 2)
#     cv2.imshow("Frame", frame)
#     if cv2.waitKey(1) & 0xFF == ord('q'):
#         break
# video_capture.release()
# cv2.destroyAllWindows()